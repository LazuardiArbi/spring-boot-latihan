<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-15
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
</head>
<body>
<c:if test="${not empty authData}">
    <h2>Update Data</h2>
    <c:url var="url" value="/updateauthinfos"/>
    <form:form method="post" action="${url}">
        Auth Client Id:<br>
        <input type="text" name="oauthClientId" value="${authData.oauthClientId}">
        <br>
        Auth Merchant Id:<br>
        <input type="text" name="oauthMerchantId" value="${authData.oauthMerchantId}">
        <br>
        Auth URL:<br>
        <input type="text" name="oauthRedirectUrl" value="${authData.oauthRedirectUrl}">
        <br>
        Auth Client Secret:<br>
        <input type="text" name="oauthClientSecret" value="${authData.oauthClientSecret}">
        <br>
        Merchant Id:<br>
        <select name="merchantId">
            <c:forEach items="${mercList}" var="mrclist">
                <option value="${mrclist.id}">${mrclist.id}</option>
            </c:forEach>
        </select>
        <br>
        Environment Id:<br>
        <select name="environmentId">
            <c:forEach items="${envList}" var="envlist">
                <option value="${envlist.id}">${envlist.id}</option>
            </c:forEach>
        </select>
        <br>
        <br>
        <input type="submit" value="Update">
    </form:form>
</c:if>
<c:if test="${empty authData}">
    <h2>Create Data</h2>
    <div>${msg}</div>
    <c:url var="url" value="/createauthinfos"/>
    <form:form method="post" action="${url}">
        Auth Client Id:<br>
        <input type="text" name="oauthClientId_">
        <br>
        Auth Merchant Id:<br>
        <input type="text" name="oauthMerchantId_">
        <br>
        Auth URL:<br>
        <input type="text" name="oauthRedirectUrl_">
        <br>
        Auth Client Secret:<br>
        <input type="text" name="oauthClientSecret_">
        <br>
        Merchant Id:<br>
        <select name="merchantId_">
            <c:forEach items="${mercList}" var="mrclist">
                <option value="${mrclist.id}">${mrclist.id}</option>
            </c:forEach>
        </select>
        <br>
        Environment Id:<br>
        <select name="environmentId_">
            <c:forEach items="${envList}" var="envlist">
                <option value="${envlist.id}">${envlist.id}</option>
            </c:forEach>
        </select>
        <br>
        <br>
        <input type="submit" value="Add">
    </form:form>
</c:if>
</body>
</html>
