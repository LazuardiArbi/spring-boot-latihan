<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-15
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dashboard</title>
</head>
<body>
<c:url var="env" value="/environments"/>
<c:url var="mrc" value="/merchants"/>
<c:url var="auth" value="/authinfos"/>

<table>
    <tr>
        <td>
            <form:form method="get" action="${env}">
                <input type="submit" value="Environment">
            </form:form>
        </td>
        <td>
            <form:form method="get" action="${mrc}">
                <input type="submit" value="Merchant">
            </form:form>
        </td>
        <td>
            <form:form method="get" action="${auth}">
                <input type="submit" value="Authentication Info">
            </form:form>
        </td>
    </tr>
</table>
</body>
</html>
