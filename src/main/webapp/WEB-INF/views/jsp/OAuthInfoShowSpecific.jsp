<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-15
  Time: 17:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Specific</title>
</head>
<body>
<c:if test="${not empty model && not empty envId && not empty mercId}">
    <c:if test="${empty model}">
        <div>${msg}</div>
    </c:if>
    <c:if test="${not empty model}">
        <table>
            <tr>
                <td>oauthMerchantId</td>
                <td>oauthClientId</td>
                <td>oauthClientSecret</td>
                <td>oauthRedirectUrl</td>
                <td>Merchent ID</td>
                <td>Merchent Name</td>
                <td>Environment ID</td>
                <td>Environment Name</td>
            </tr>
            <tr>
                <td>${model.oauthMerchantId}</td>
                <td>${model.oauthClientId}</td>
                <td>${model.oauthClientSecret}</td>
                <td>${model.oauthRedirectUrl}</td>
                <td>${mercId.id}</td>
                <td>${mercId.name}</td>
                <td>${envId.id}</td>
                <td>${envId.name}</td>
            </tr>
        </table>
    </c:if>
</c:if>
</body>
</html>
