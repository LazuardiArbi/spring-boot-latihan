<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-13
  Time: 17:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View</title>
</head>
<body>
<c:url var="dashboard" value="/dashboard"/>
<%--Untuk environment--%>
<c:if test="${env != null}">
    <c:url var="delete" value="/environments/delete"/>
    <c:url var="update" value="/environments/update"/>
    <c:url var="viewData" value="/environments"/>
    <c:url var="createData" value="/environments/create"/>
    <c:if test="${not empty env}">
        <table>
            <tr>
                <th><b>ID</b></th>
                <th><b>NAME</b></th>
                <th></th>
            </tr>

            <c:forEach items="${env}" var="list">
                <tr>
                    <td>${list.id}</td>
                    <td>${list.name}</td>
                    <td>
                        <form:form method="get" action="${update}/${list.id}">
                            <input type="submit" value="Update">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="get" action="${delete}/${list.id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="get" action="${viewData}/${list.id}">
                            <input type="submit" value="View">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${empty env}">
        <div>There's no Data</div>
    </c:if>
    <table>
        <tr>
            <td>
                <form:form method="get" action="${createData}">
                    <input type="submit" value="Add New Environment">
                </form:form>
            </td>
            <td>
                <form:form method="get" action="${dashboard}">
                    <input type="submit" value="Back to Dashboard">
                </form:form>
            </td>
        </tr>
    </table>
</c:if>

<%--Untuk merchant--%>
<c:if test="${merc != null}">
    <c:url var="delete" value="/merchants/delete"/>
    <c:url var="update" value="/merchants/update"/>
    <c:url var="viewData" value="/merchants"/>
    <c:url var="createData" value="/merchants/create"/>

    <c:if test="${not empty merc}">
        <table>
            <tr>
                <th><b>ID</b></th>
                <th><b>NAME</b></th>
                <th></th>
            </tr>

            <c:forEach items="${merc}" var="mercList">
                <tr>
                    <td>${mercList.id}</td>
                    <td>${mercList.name}</td>
                    <td>
                        <form:form method="get" action="${update}/${mercList.id}">
                            <input type="submit" value="Update">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="get" action="${delete}/${mercList.id}">
                            <input type="submit" value="Delete">
                        </form:form>
                    </td>
                    <td>
                        <form:form method="get" action="${viewData}/${mercList.id}">
                            <input type="submit" value="View">
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${empty merc}">
        <div>There's no Data</div>
    </c:if>
    <table>
        <tr>
            <td>
                <form:form method="get" action="${createData}">
                    <input type="submit" value="Add New Merchant">
                </form:form>
            </td>
            <td>
                <form:form method="get" action="${dashboard}">
                    <input type="submit" value="Back to Dashboard">
                </form:form>
            </td>
        </tr>
    </table>
</c:if>

</body>
</html>
