<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-15
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View</title>
</head>
<body>
<c:url var="dashboard" value="/dashboard"/>
<c:url var="createData" value="/authinfos/create"/>

<c:if test="${not empty auth}">
    <c:url var="delete" value="/authinfos/delete"/>
    <c:url var="update" value="/authinfos/update"/>
    <c:url var="viewData" value="/authinfos"/>
    <table>
        <h3>OAuthInfos</h3>
        <tr>
            <th><b>MerchantId</b></th>
            <th><b>oauthClientId</b></th>
            <th><b>oauthClientSecret</b></th>
            <th><b>oauthRedirectUrl</b></th>
            <th><b>merchantId(FK)</b></th>
            <th><b>environmentId(FK)</b></th>
        </tr>

        <c:forEach items="${auth}" var="list">
            <tr>
                <td>${list.oauthMerchantId}</td>
                <td>${list.oauthClientId}</td>
                <td>${list.oauthClientSecret}</td>
                <td>${list.oauthRedirectUrl}</td>
                <td>${list.merchantId}</td>
                <td>${list.environmentId}</td>
                <td>
                    <form:form method="get" action="${update}/${list.oauthClientId}">
                        <input type="submit" value="Update">
                    </form:form>
                </td>
                <td>
                    <form:form method="get" action="${delete}/${list.oauthClientId}">
                        <input type="submit" value="Delete">
                    </form:form>
                </td>
                <td>
                    <form:form method="get" action="${viewData}/${list.oauthClientId}">
                        <input type="submit" value="View">
                    </form:form>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty auth}">
    <div>There's no Data</div>
</c:if>
<table>
    <tr>
        <td>
            <form:form method="get" action="${createData}">
                <input type="submit" value="Add New Authentication Info">
            </form:form>
        </td>
        <td>
            <form:form method="get" action="${dashboard}">
                <input type="submit" value="Back to Dashboard">
            </form:form>
        </td>
    </tr>
</table>
</body>
</html>
