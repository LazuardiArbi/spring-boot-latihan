<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lazuardi.rahimarbiputro
  Date: 2019-03-14
  Time: 09:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Specific</title>
</head>
<body>
<c:if test="${empty model}">
    <div>${msg}</div>
</c:if>
<c:if test="${not empty model}">
    <table>
        <tr>
            <td>ID</td>
            <td>Name</td>
        </tr>
        <tr>
            <td>${model.id}</td>
            <td>${model.name}</td>
        </tr>
    </table>
</c:if>
</body>
</html>
