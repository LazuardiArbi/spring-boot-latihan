package com.mkyong.web.dto; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginDto.java, v 0.1 2019-03-15 15:36 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public class LoginDto {
    private String username;
    private String password;

    /**
     * Getter method for property username.
     *
     * @return property value of username
     **/
    public String getUsername() {
        return username;
    }

    /**
     * Setter method for property username.
     *
     * @param username value to be assigned to property username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter method for property password.
     *
     * @return property value of password
     **/
    public String getPassword() {
        return password;
    }

    /**
     * Setter method for property password.
     *
     * @param password value to be assigned to property password
     */
    public void setPassword(String password) {
        this.password = password;
    }
}