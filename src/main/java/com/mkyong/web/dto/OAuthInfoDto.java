package com.mkyong.web.dto; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoDto.java, v 0.1 2019-03-13 17:58 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public class OAuthInfoDto {
    private String merchantId;
    private String environmentId;
    private String oauthMerchantId;
    private String oauthClientId;
    private String oauthClientSecret;
    private String oauthRedirectUrl;

    /**
     * Getter method for property oauthMerchantId.
     *
     * @return property value of oauthMerchantId
     **/
    public String getOauthMerchantId() {
        return oauthMerchantId;
    }

    /**
     * Setter method for property oauthMerchantId.
     *
     * @param oauthMerchantId value to be assigned to property oauthMerchantId
     */
    public void setOauthMerchantId(String oauthMerchantId) {
        this.oauthMerchantId = oauthMerchantId;
    }

    /**
     * Getter method for property oauthClientId.
     *
     * @return property value of oauthClientId
     **/
    public String getOauthClientId() {
        return oauthClientId;
    }

    /**
     * Setter method for property oauthClientId.
     *
     * @param oauthClientId value to be assigned to property oauthClientId
     */
    public void setOauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
    }

    /**
     * Getter method for property oauthClientSecret.
     *
     * @return property value of oauthClientSecret
     **/
    public String getOauthClientSecret() {
        return oauthClientSecret;
    }

    /**
     * Setter method for property oauthClientSecret.
     *
     * @param oauthClientSecret value to be assigned to property oauthClientSecret
     */
    public void setOauthClientSecret(String oauthClientSecret) {
        this.oauthClientSecret = oauthClientSecret;
    }

    /**
     * Getter method for property oauthRedirectUrl.
     *
     * @return property value of oauthRedirectUrl
     **/
    public String getOauthRedirectUrl() {
        return oauthRedirectUrl;
    }

    /**
     * Setter method for property oauthRedirectUrl.
     *
     * @param oauthRedirectUrl value to be assigned to property oauthRedirectUrl
     */
    public void setOauthRedirectUrl(String oauthRedirectUrl) {
        this.oauthRedirectUrl = oauthRedirectUrl;
    }

    /**
     * Getter method for property merchantId.
     *
     * @return property value of merchantId
     **/
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * Setter method for property merchantId.
     *
     * @param merchantId value to be assigned to property merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * Getter method for property environmentId.
     *
     * @return property value of environmentId
     **/
    public String getEnvironmentId() {
        return environmentId;
    }

    /**
     * Setter method for property environmentId.
     *
     * @param environmentId value to be assigned to property environmentId
     */
    public void setEnvironmentId(String environmentId) {
        this.environmentId = environmentId;
    }
}