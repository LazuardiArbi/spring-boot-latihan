package com.mkyong.web.dao.OAuthInfo; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.OAuthInfoDto;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoDaoImpl.java, v 0.1 2019-03-14 15:25 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Repository
public class OAuthInfoDaoImpl implements OAuthInfoDao {

    @Override
    public void createOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.insert("oauthinfo.addOAuthInfo", oAuthInfoDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.update("oauthinfo.updateOAuthInfoByClientId", oAuthInfoDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOAuthInfo(String id, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.delete("oauthinfo.deleteOAuthInfoByClientId", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public OAuthInfoDto getOAuthInfoById(String id, SqlMapClient sqlMapClient) {
        try {
            OAuthInfoDto user = (OAuthInfoDto) sqlMapClient.queryForObject("oauthinfo.getOAuthInfoByClientId", id);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<OAuthInfoDto> getOAuthInfo(SqlMapClient sqlMapClient) {
        try {
            List<OAuthInfoDto> result = (List<OAuthInfoDto>) sqlMapClient.queryForList("oauthinfo.getAllOAuthInfo");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}