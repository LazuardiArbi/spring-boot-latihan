package com.mkyong.web.dao.OAuthInfo; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.OAuthInfoDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoDao.java, v 0.1 2019-03-14 15:23 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface OAuthInfoDao {

    void createOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient);

    void updateOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient);

    void deleteOAuthInfo(String id, SqlMapClient sqlMapClient);

    OAuthInfoDto getOAuthInfoById(String id, SqlMapClient sqlMapClient);

    List<OAuthInfoDto> getOAuthInfo(SqlMapClient sqlMapClient);
}