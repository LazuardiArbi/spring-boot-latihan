package com.mkyong.web.dao.LoginDao; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.LoginDto;
import org.springframework.stereotype.Repository;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginDaoImpl.java, v 0.1 2019-03-15 15:32 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Repository
public class LoginDaoImpl implements LoginDao{
    @Override
    public LoginDto validateLogin(LoginDto loginDto, SqlMapClient sqlMapClient) {
        try {
            LoginDto user = (LoginDto) sqlMapClient.queryForObject("login.getLogin", loginDto);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}