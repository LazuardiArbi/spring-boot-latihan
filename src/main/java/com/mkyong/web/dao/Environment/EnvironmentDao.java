package com.mkyong.web.dao.Environment; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.EnvironmentDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: EnvironmentDao.java, v 0.1 2019-03-13 18:33 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */

public interface EnvironmentDao {
    void createEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient);

    void updateEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient);

    void deleteEnvironment(String id, SqlMapClient sqlMapClient);

    EnvironmentDto getEnvironmentById(String id, SqlMapClient sqlMapClient);

    List<EnvironmentDto> getEnvironment(SqlMapClient sqlMapClient);
}