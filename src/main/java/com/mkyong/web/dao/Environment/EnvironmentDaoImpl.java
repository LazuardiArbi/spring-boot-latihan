package com.mkyong.web.dao.Environment; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.EnvironmentDto;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: EnvironmentDaoImpl.java, v 0.1 2019-03-13 20:16 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Repository
public class EnvironmentDaoImpl implements EnvironmentDao {

    @Override
    public void createEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.insert("environment.addEnvironment", environmentDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.update("environment.updateEnvironmentById", environmentDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteEnvironment(String id, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.delete("environment.deleteEnvironmentById", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public EnvironmentDto getEnvironmentById(String id, SqlMapClient sqlMapClient) {
        try {
            EnvironmentDto user = (EnvironmentDto) sqlMapClient.queryForObject("environment.getEnvironmentById", id);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<EnvironmentDto> getEnvironment(SqlMapClient sqlMapClient) {
        try {
            List<EnvironmentDto> result = (List<EnvironmentDto>) sqlMapClient.queryForList("environment.getAllEnvironment");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}