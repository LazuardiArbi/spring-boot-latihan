package com.mkyong.web.dao.Merchent; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.MerchantDto;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: MerchantDaoImpl.java, v 0.1 2019-03-14 14:09 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Repository
public class MerchantDaoImpl implements MerchantDao {

    @Override
    public void createMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.insert("merchant.addMerchent", merchantDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.update("merchant.updateMerchentById", merchantDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteMerchant(String id, SqlMapClient sqlMapClient) {
        try {
            sqlMapClient.delete("merchant.deleteMerchentById", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public MerchantDto getMerchantById(String id, SqlMapClient sqlMapClient) {
        try {
            MerchantDto user = (MerchantDto) sqlMapClient.queryForObject("merchant.getMerchentById", id);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<MerchantDto> getMerchant(SqlMapClient sqlMapClient) {
        try {
            List<MerchantDto> result = (List<MerchantDto>) sqlMapClient.queryForList("merchant.getAllMerchent");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}