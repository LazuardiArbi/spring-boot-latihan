package com.mkyong.web.dao.Merchent; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.MerchantDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: MerchantDao.java, v 0.1 2019-03-14 14:06 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface MerchantDao {
    void createMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient);

    void updateMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient);

    void deleteMerchant(String id, SqlMapClient sqlMapClient);

    MerchantDto getMerchantById(String id, SqlMapClient sqlMapClient);

    List<MerchantDto> getMerchant(SqlMapClient sqlMapClient);
}