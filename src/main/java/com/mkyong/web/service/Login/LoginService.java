package com.mkyong.web.service.Login; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.LoginDto;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: LoginService.java, v 0.1 2019-03-15 15:35 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface LoginService {
    LoginDto findUsernameAndPassword(LoginDto loginDto, SqlMapClient sqlMapClient);
}