package com.mkyong.web.service.Environment; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.EnvironmentDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: EnvironmentService.java, v 0.1 2019-03-13 21:34 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface EnvironmentService {

    EnvironmentDto findByEnvironmentId(String id, SqlMapClient sqlMapClient);

    List<EnvironmentDto> getAllEnvironment(SqlMapClient sqlMapClient);

    void deleteByEnvironmentId(String id, SqlMapClient sqlMapClient);

    void createEnvironment(EnvironmentDto environmentDto, SqlMapClient sqlMapClient);

    void updateByEnvironmentId(EnvironmentDto environmentDto, SqlMapClient sqlMapClient);
}