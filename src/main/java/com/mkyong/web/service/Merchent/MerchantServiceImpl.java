package com.mkyong.web.service.Merchent; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dao.Merchent.MerchantDao;
import com.mkyong.web.dto.MerchantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: MerchantServiceImpl.java, v 0.1 2019-03-14 14:42 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Service
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    MerchantDao merchantDao;

    @Override
    public MerchantDto findByMerchantId(String id, SqlMapClient sqlMapClient) {
        return merchantDao.getMerchantById(id, sqlMapClient);
    }

    @Override
    public List<MerchantDto> getAllMerchant(SqlMapClient sqlMapClient) {
        return merchantDao.getMerchant(sqlMapClient);
    }

    @Override
    public void deleteByMerchantId(String id, SqlMapClient sqlMapClient) {
        merchantDao.deleteMerchant(id, sqlMapClient);
    }

    @Override
    public void createMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient) {
        merchantDao.createMerchant(merchantDto, sqlMapClient);
    }

    @Override
    public void updateByMerchantId(MerchantDto merchantDto, SqlMapClient sqlMapClient) {
        merchantDao.updateMerchant(merchantDto, sqlMapClient);
    }
}