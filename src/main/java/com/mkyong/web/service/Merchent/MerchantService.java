package com.mkyong.web.service.Merchent; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dto.MerchantDto;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: MerchantService.java, v 0.1 2019-03-14 14:46 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
public interface MerchantService {
    MerchantDto findByMerchantId(String id, SqlMapClient sqlMapClient);

    List<MerchantDto> getAllMerchant(SqlMapClient sqlMapClient);

    void deleteByMerchantId(String id, SqlMapClient sqlMapClient);

    void createMerchant(MerchantDto merchantDto, SqlMapClient sqlMapClient);

    void updateByMerchantId(MerchantDto merchantDto, SqlMapClient sqlMapClient);
}