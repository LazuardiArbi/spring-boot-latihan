package com.mkyong.web.service.OAuthInfo; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.sqlmap.client.SqlMapClient;
import com.mkyong.web.dao.OAuthInfo.OAuthInfoDao;
import com.mkyong.web.dto.OAuthInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: OAuthInfoServiceImpl.java, v 0.1 2019-03-14 15:41 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Service
public class OAuthInfoServiceImpl implements OAuthInfoService{

    @Autowired
    OAuthInfoDao oAuthInfoDao;

    @Override
    public OAuthInfoDto findByOAuthInfoId(String id, SqlMapClient sqlMapClient) {
        return oAuthInfoDao.getOAuthInfoById(id, sqlMapClient);
    }

    @Override
    public List<OAuthInfoDto> getAllOAuthInfo(SqlMapClient sqlMapClient) {
        System.out.println(oAuthInfoDao.getOAuthInfo(sqlMapClient));
        return oAuthInfoDao.getOAuthInfo(sqlMapClient);
    }

    @Override
    public void deleteByOAuthInfoId(String id, SqlMapClient sqlMapClient) {
        oAuthInfoDao.deleteOAuthInfo(id, sqlMapClient);
    }

    @Override
    public void createOAuthInfo(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient) {
        oAuthInfoDao.createOAuthInfo(oAuthInfoDto, sqlMapClient);
    }

    @Override
    public void updateByOAuthInfoId(OAuthInfoDto oAuthInfoDto, SqlMapClient sqlMapClient) {
        oAuthInfoDao.updateOAuthInfo(oAuthInfoDto, sqlMapClient);
    }
}