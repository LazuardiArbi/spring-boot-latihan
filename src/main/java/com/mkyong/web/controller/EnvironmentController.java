package com.mkyong.web.controller;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mkyong.web.dto.EnvironmentDto;
import com.mkyong.web.service.Environment.EnvironmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

@Controller
public class EnvironmentController {

    /**
     * # = Berhasil jalan
     * O = Belum dicoba
     */

    @Autowired
    EnvironmentService environmentService;

    /**
     * Get All Environment Data #
     */
    @RequestMapping(value = "/environments", method = RequestMethod.GET)
    public String ShowAllEnvironment(Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        List<EnvironmentDto> environmentDtoList = environmentService.getAllEnvironment(sqlmapClient);

        System.out.println(environmentDtoList);

        if (environmentDtoList.isEmpty()) {
            model.addAttribute("msg", "Data is Empty");
        }
        model.addAttribute("env", environmentDtoList);

        return "view";
    }

    /**
     * Get Environment Data by Id #
     */
    @RequestMapping(value = "/environments/{id}", method = RequestMethod.GET)
    public String ShowEnvironmetById(Model model, @PathVariable String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        EnvironmentDto environmentDto = environmentService.findByEnvironmentId(id, sqlmapClient);

        if (environmentDto == null) {
            model.addAttribute("msg", "User Not Found");
        }
        model.addAttribute("model", environmentDto);

        return "showSpecific";
    }

    /**
     * Delete Data by Id and it will redirect to /environments (dashboard) #
     */
    @RequestMapping(value = "/environments/delete/{id}", method = RequestMethod.GET)
    public String DeleteEnvironmentById(@PathVariable("id") String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        environmentService.deleteByEnvironmentId(id, sqlmapClient);
        return "redirect:/environments";
    }

    /**
     * Show update Form #
     */
    @RequestMapping(value = "/environments/update/{id}", method = RequestMethod.GET)
    public String ShowFormUpdateEnvironmentById(@PathVariable String id, Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        EnvironmentDto environmentDto = environmentService.findByEnvironmentId(id, sqlmapClient);
        model.addAttribute("envData", environmentDto);
        return "EnvironmentForm";
    }

    /**
     * Update Data #
     */
    @RequestMapping(value = "/updateEnvironments", method = RequestMethod.POST)
    public String UpdateEnvironmentById(EnvironmentDto environmentDto, BindingResult result, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        if (result.hasErrors()) {
            return "redirect:/environments/update/" + environmentDto.getId();
        } else {
            String id = request.getParameter("id");
            String name = request.getParameter("name");

            System.out.println(id + name);
            if (id.isEmpty() == true || name.isEmpty() == true) {
                return "redirect:/environments/update/" + environmentDto.getId();
            }

            environmentDto.setId(id);
            environmentDto.setName(name.toUpperCase());

            environmentService.updateByEnvironmentId(environmentDto, sqlmapClient);
            return "redirect:/environments";
        }
    }

    /**
     * Show Create Form #
     */
    @RequestMapping(value = "/environments/create", method = RequestMethod.GET)
    public String ShowFormCreateEnvironment() {
        return "EnvironmentForm";
    }

    /**
     * push data #
     */
    @RequestMapping(value = "/createEnvironments", method = RequestMethod.POST)
    public String CreateEnvironment(Model model, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        EnvironmentDto environmentDto = new EnvironmentDto();

        String id = request.getParameter("id_");
        String name = request.getParameter("name_");

        if (environmentService.findByEnvironmentId(id, sqlmapClient) != null) {
            model.addAttribute("msg", "Data ID sudah ada sebelumnya");
            return "EnvironmentForm";
        } else if (id.isEmpty() == true && name.isEmpty() == true) {
            model.addAttribute("msg", "ID / Nama tidak boleh kosong");
            return "EnvironmentForm";
        }

        environmentDto.setName(name.toUpperCase());
        environmentDto.setId("E" + id);
        environmentService.createEnvironment(environmentDto, sqlmapClient);
        return "redirect:/environments";
    }
}