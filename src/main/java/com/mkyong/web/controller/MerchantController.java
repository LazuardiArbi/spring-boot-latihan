package com.mkyong.web.controller; /**
 * DANA.id
 * Copyright (c) 2004-2019 ALl Rights Reserved.
 */

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.mkyong.web.dto.MerchantDto;
import com.mkyong.web.service.Merchent.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * @author Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 * @version $Id: MerchantController.java, v 0.1 2019-03-15 11:04 Lazuardi Rahim Arbiputro (lazuardi.arbiputro@dana.id)
 */
@Controller
public class MerchantController {

    /**
     * # = Berhasil jalan
     * O = Belum dicoba
     */

    @Autowired
    MerchantService merchantService;

    /**
     * Get All Merchant Data #
     */
    @RequestMapping(value = "/merchants", method = RequestMethod.GET)
    public String ShowAllMerchant(Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        List<MerchantDto> merchantDtoList = merchantService.getAllMerchant(sqlmapClient);

        System.out.println(merchantDtoList);

        if (merchantDtoList.isEmpty()) {
            model.addAttribute("msg", "Data is Empty");
        }
        model.addAttribute("merc", merchantDtoList);

        return "view";
    }

    /**
     * Get Merchant Data by Id #
     */
    @RequestMapping(value = "/merchants/{id}", method = RequestMethod.GET)
    public String ShowMerchantById(Model model, @PathVariable String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        MerchantDto merchantDto = merchantService.findByMerchantId(id, sqlmapClient);
        if (merchantDto == null) {
            model.addAttribute("msg", "User Not Found");
        }
        System.out.println(merchantDto.getId() + merchantDto.getName());
        model.addAttribute("model", merchantDto);

        return "showSpecific";
    }

    /**
     * Delete Data by Id and it will redirect to /merchants (dashboard) #
     */
    @RequestMapping(value = "/merchants/delete/{id}", method = RequestMethod.GET)
    public String DeleteMerchantById(@PathVariable("id") String id) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        merchantService.deleteByMerchantId(id, sqlmapClient);
        return "redirect:/merchants";
    }

    /**
     * Show update Form #
     */
    @RequestMapping(value = "/merchants/update/{id}", method = RequestMethod.GET)
    public String ShowFormUpdateMerchantById(@PathVariable String id, Model model) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        MerchantDto merchantDto = merchantService.findByMerchantId(id, sqlmapClient);
        model.addAttribute("mercData", merchantDto);
        return "MerchantForm";
    }

    /**
     * Update Data #
     */
    @RequestMapping(value = "/updateMerchants", method = RequestMethod.POST)
    public String UpdateMerchantById(MerchantDto merchantDto, BindingResult result, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        if (result.hasErrors()) {
            return "redirect:/merchants/update/" + merchantDto.getId();
        } else {
            String id = request.getParameter("id");
            String name = request.getParameter("name");

            System.out.println(id + name);
            if (id.isEmpty() == true || name.isEmpty() == true) {
                return "redirect:/merchants/update/" + merchantDto.getId();
            }

            merchantDto.setId(id);
            merchantDto.setName(name.toUpperCase());

            merchantService.updateByMerchantId(merchantDto, sqlmapClient);
            return "redirect:/merchants";
        }
    }

    /**
     * Show Create Form #
     */
    @RequestMapping(value = "/merchants/create", method = RequestMethod.GET)
    public String ShowFormCreateMerchant() {
        return "MerchantForm";
    }

    /**
     * push data #
     */
    @RequestMapping(value = "/createMerchants", method = RequestMethod.POST)
    public String CreateMerchant(Model model, HttpServletRequest request) throws IOException {
        String resource = "sql-maps-config.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlMapClient sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);

        MerchantDto merchantDto = new MerchantDto();

        String id = request.getParameter("id_");
        String name = request.getParameter("name_");

        if (merchantService.findByMerchantId(id, sqlmapClient) != null) {
            model.addAttribute("msg", "Data ID sudah ada sebelumnya");
            return "MerchantForm";
        } else if (id.isEmpty() == true && name.isEmpty() == true) {
            model.addAttribute("msg", "ID / Nama tidak boleh kosong");
            return "MerchantForm";
        }

        merchantDto.setName(name.toUpperCase());
        merchantDto.setId("M" + id);
        merchantService.createMerchant(merchantDto, sqlmapClient);
        return "redirect:/merchants";
    }
}